'use strict';

const onGameChanged = (selector) => {
  setupTable();
}

const setupGameSelector = () => {
  const selector = document.getElementById('game-select'); 
  Object.keys(GAMES).forEach(g => {
    const o = document.createElement('option');
	o.innerText = GAMES[g].name;
	o.value = g;
	selector.appendChild(o);
  });
}

const shuffle = (array) => {
  let currentIndex = array.length;
  let temporaryValue;
  let randomIndex;
  while (0 !== currentIndex) {
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }
  
  // Set the free space after shuffle.
  let tmp = array[12];
  array[12] = 'Free Space!';
  array.push(tmp);
  console.log('done');
  return array;
}

const getGameSelectorValue = () => {
  const s = document.getElementById('game-select');
  return s.options[s.selectedIndex].value;
}

const clearTable = () => {
  const t = document.getElementById('game-table');
  const numRows = t.rows.length - 1;
  for(let i = numRows; i > 0; i--) {
    t.deleteRow(i);
  }
}

const onCellClicked = (elem) => {
  if (elem.classList.contains('selected')) {
    elem.classList.remove('selected');
  } else {
    elem.classList.add('selected');
  }
}

const generateCell = (text, selected) => {
  let cell = document.createElement('td');
  cell.name = 'gameTile';
  cell.innerText = text ? text : '';
  if (selected) {
    cell.classList.add('selected');
  } else {
    cell.onclick = () => onCellClicked(cell);
  }
  return cell;
}

const generateRow = (tiles, elem, rowNum) => {
  let row = document.createElement('tr');
  for (let col = 1; col < 6; col++) {
    let index = (rowNum * 5) + col - 1;
    row.appendChild(generateCell(tiles[index], index === 12));
  }
  return row;
}

const setupTable = () => {
  clearTable();
  const tiles = [].concat(GAMES[getGameSelectorValue()].values);
  const shuffled = shuffle(tiles);
  const elem = document.getElementById("table-body");
  for (let row = 0; row < 5; row++) {
    elem.appendChild(generateRow(shuffled, elem, row));
  }
}

(function () {
  setupGameSelector();
  setupTable();
})();
