'use strict';

const GAMES = {
  smite: {
    name: 'Smite',
    values: [
      'Meditation on a mage',
      'Racial or homophobic slur',
      'No relic',
      'Stacking item build 3rd or later',
      'Ult to show minion dominance ',
      'Change, pers or aphro',
      'Dying to minions or tower (no god credit)',
      'Diving a tower and dying pre level 6',
      'Bronze flare',
      'Full dmg tank',
      'No beads against ares/cerb',
      'Overstay ville train ticket',
      'Vgs “ok” train',
      'No god selected',
      'To many stars to count',
      'No antiheal items',
      'Wtf is that build',
      'The Zapman jump or dash in on ADC',
      'We already have a mage',
      'Autolock newest god',
      'Rando skin booster',
      'Beads and aegis',
      'Spam laugh',
      'Someone disconnects'
    ]	
  }
}
